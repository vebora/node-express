var fs = require('fs');
var dateFormat = require('dateformat');
var child_process = require('child_process')

child_process.exec('git rev-parse --short HEAD', function (err, stdout) {
  const rev = stdout.trim();
  const newDate = new Date(new Date().getTime() + 3 * 60 * 60 * 1000);
  const date = dateFormat(newDate,"GMT:yyyy-mm-dd HH:MM:ss");
  console.log("Build date " + date);
  const msg =
    "export class BuildInfo {\n public static readonly date = '" + date + "';\n" +
    " public static readonly rev = '" + rev + "';\n}\n";


  fs.writeFile("src/buildinfo.ts", msg, function (err) {
    if (err) {
      return console.log(err);
    }
  });

});
