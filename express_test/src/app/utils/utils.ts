import { User } from '../models/user';

export default class Util {

  static createUniqueId(id1: string, id2: string): string {

    if (id1 > id2) {
      return id1 + id2;
    } else {
      return id2 + id1;
    }
  }

  static getOtherUser(user1: User, user2: User, currentUser: User): User {

    if (user1.id === currentUser.id) {
      return user2;
    } else {
      return user1;
    }
  }

  // THIS FUNCTION IS VERY VERY DANGEROUS. CONSIDER CHANGING THE WAY HOW TO FIND OTHER USER ID
  static getOtherUserId(roomId: string, currentUserId) {
    return roomId.replace(currentUserId, '');
  }
}
