import { Component, OnInit } from '@angular/core';
import { routerTransition } from './animations/router.animations';
import { SwUpdate } from '@angular/service-worker';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  animations: [routerTransition],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private sw: SwUpdate,
    private ms: MatSnackBar) { }

  ngOnInit() {
    this.sw.available.subscribe((event) => {
      const snackBarRef = this.ms.open('Uygulamanızın yeni sürümü var', 'YENILE', { duration: 3000000 });
      snackBarRef.onAction().subscribe(() => {
        window.location.reload();
      });
    });

    // document.getElementById('shell').style.display = 'none';
  }

  getState(outlet) {
    return outlet.activatedRouteData.state;
  }
}



