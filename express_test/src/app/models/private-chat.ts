import { User } from './user';
import { ChatMessage } from './chat-message';

export class PrivateChat {
  fromUser: User;
  toUser: User;
  messages?: ChatMessage[];
}
