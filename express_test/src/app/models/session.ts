import * as firebase from 'firebase/app';

export interface Session {
  timestamp: firebase.firestore.Timestamp;
  user_agent: string;
}
