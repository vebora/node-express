
export interface ChatMessage {
  message: string;
  timestamp: any;
  username: string;
  photo: string;
  roomId: string;
  userId: string;
}
