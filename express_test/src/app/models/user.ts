import * as firebase from 'firebase/app';


export interface User {
  id: string;
  photoUrl: string;
  email: string;
  name: string;
  phoneNumber: string;
  lastLogin: firebase.firestore.Timestamp;
  admin?: boolean;
}
