import * as firebase from 'firebase/app';

export interface ChatRoom {
  id: string;
  roomName: string;
  photoUrl: string;
  lastMessage: MessageSummary;
  hasUnread: boolean;
}
interface MessageSummary {
  message: string;
  timestamp: firebase.firestore.Timestamp;
}
