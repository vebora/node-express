import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';


@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private as: AuthService, private ms: MatSnackBar) { }

  canActivate() {
    return this.as.user$.pipe(
      map(user => {
        if (user) {
          return true;
        } else {
          this.ms.open('Bu sayfaya erişmek için giriş yapmanız gerekmektedir');
          this.router.navigate(['/login']);
          return false;
        }
      }),
      catchError((err) => {
        this.ms.open('Bir hata oluştu : AuthGuard');
        return of(false);
      })
    );
  }
}
