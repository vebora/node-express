import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  _loading: BehaviorSubject<boolean> = new BehaviorSubject(false);
  _loading$ = this._loading.asObservable();
  constructor() {
    console.log('Initializing LoadingService');

  }

  get loading(): Observable<boolean> {
    return this._loading$;
  }

  show() {
    this._loading.next(true);
  }

  hide() {
    this._loading.next(false);
  }

}
