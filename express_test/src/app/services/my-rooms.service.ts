import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthService } from './auth.service';
import { LoadingService } from './loading.service';
import { Subject, BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ChatRoom } from '../models/chat-room';

@Injectable()
export class MyRoomsService {

  private _roomsSubject: Subject<ChatRoom[]> = new ReplaySubject(1);
  private _room$: Observable<ChatRoom[]> = this._roomsSubject.asObservable();

  constructor(
    afs: AngularFirestore,
    as: AuthService,
    loading: LoadingService
  ) {
    as.user$.subscribe(user => {
      if (user) {
        loading.show();
        afs.collection('users').doc(as.user.id).collection<ChatRoom>('rooms',
          ref => ref.orderBy('lastMessage.timestamp', 'desc')).valueChanges().pipe(
            tap(() => loading.hide()),
        ).subscribe(rooms => this._roomsSubject.next(rooms));
      }
    });
  }

  get rooms$() {
    return this._room$;
  }
}
