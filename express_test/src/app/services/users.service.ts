import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { AngularFirestore } from 'angularfire2/firestore';
import { LoadingService } from './loading.service';
import { User } from '../models/user';
import { filter, tap, map } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable()
export class UsersService {

  _usersSubject: Subject<User[]> = new BehaviorSubject([]);
  _users$ = this._usersSubject.asObservable();

  constructor(
    private afs: AngularFirestore,
    loadingService: LoadingService,
    authService: AuthService
  ) {
    loadingService.show();
    this.afs.collection<User>('users', ref => ref.orderBy('lastLogin', 'desc')).valueChanges()
      .pipe(
        map(userList => {
          if (authService.user.admin) {
            return userList;
          } else {
            return userList.filter((u: User) => !u.admin);
          }
        }),
    )
      .subscribe(sessions => {
        this._usersSubject.next(sessions);
        loadingService.hide();
      });

  }

  get users$() {
    return this._users$;
  }
}
