import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { AuthService } from './auth.service';
import { AngularFirestore } from 'angularfire2/firestore';
import { Title } from '@angular/platform-browser';


@Injectable()
export class NotificationService {

  private _notificationCount: Subject<number> = new BehaviorSubject(0);
  private _notificationCount$ = this._notificationCount.asObservable();
  private title: string;
  constructor(
    private authService: AuthService,
    private afs: AngularFirestore,
    titleService: Title
  ) {
    this.title = titleService.getTitle();

    this.authService.user$.subscribe(user => {
      if (user) {
        this.afs.collection('users').doc(user.id).collection('rooms', ref => ref.where('hasUnread', '==', true)).valueChanges().pipe(
          tap(userRooms => {
            this._notificationCount.next(userRooms.length);
          })
        ).subscribe();

        this._notificationCount$.subscribe(count => {
          if (count > 0) {
            titleService.setTitle(this.title + '(' + count + ')');
          } else {
            titleService.setTitle(this.title);
          }
        });

      }
    });
  }

  get notificationCount$(): Observable<number> {
    return this._notificationCount$;
  }
}


 // Finds unread message count
       // this.afs.collection('users').doc(user.id).collection('rooms').valueChanges().pipe(
        //   tap(() => console.log('Checking unread msg')),
        //   tap(() => this.clearCounter()),
        //   concatAll(),
        //   filter(room => room.id !== undefined),
        //   flatMap(room => this.afs.collection('rooms').doc(room.id).collection<ChatMessage>('messages',
        //     ref => {
        //       if (room.lastRead) {
        //         return ref.where('timestamp', '>', room.lastRead);
        //       } else {
        //         return ref;
        //       }
        //     }).valueChanges().pipe(take(1))),
        //   concatAll(),
        //   filter((msg => msg.userId !== user.id)),
        //   tap(data => {
        //     // console.log(data);
        //     this.increaseCounter();
        //   }),
        // ).subscribe();


        // this.afs.collection('users').doc(user.id).collection('rooms').valueChanges().pipe(
        //   tap(userRooms => {
        //     console.log('Checking unreads');
        //     let counter = 0;
        //     userRooms.forEach(userRoom => {
        //       this.afs.collection('rooms').doc<any>(userRoom.id).valueChanges().pipe(
        //         take(1),
        //         tap(roomDoc => {
        //           if (roomDoc.lastMessage) {
        //             if (!userRoom.lastRead) {
        //               counter++;
        //             } else if (userRoom.lastRead < roomDoc.lastMessage.timestamp) {
        //               counter++;
        //             }
        //             this._notificationCount.next(counter);

        //           }
        //         })
        //       ).subscribe();
        //     });
        //   })
        // ).subscribe();
