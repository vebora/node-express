import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';


import * as firebase from 'firebase/app';
import { Subject, ReplaySubject, } from 'rxjs';
import { environment } from '../../environments/environment';
import * as firebaseui from 'firebaseui';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private _loginSubject: Subject<any> = new ReplaySubject(1);
  private _loginSubject$ = this._loginSubject.asObservable();

  private ui;
  constructor(
    private afAuth: AngularFireAuth,
    private router: Router

  ) {
    console.log('LoginService start');

    afAuth.authState.subscribe(state => {
      this._loginSubject.next(state);
    });

    this.ui = new firebaseui.auth.AuthUI(this.afAuth.auth);
    this.ui.disableAutoSignIn();

  }

  get login$() {
    return this._loginSubject$;
  }
  retrieve() {
    this.ui.start('#firebaseui-container', config);
  }

  logOut() {
    this.afAuth.auth.signOut();
  }

  loginAsAdmin() {
    console.log('Loggin as admin');

    this.afAuth.auth.signInWithEmailAndPassword('admin@me.com', '123123123a');
  }

  saveUserToLocalStorage(user) {
    localStorage.setItem('user', JSON.stringify(user));
  }
}

const config = {
  'callbacks': { 'signInSuccessWithAuthResult': function (currentUser, credential, redirectUrl) { return false; } },
  // Opens IDP Providers sign-in flow in a popup.
  'signInFlow': 'redirect',
  'signInOptions': [
    // TODO(developer): Remove the providers you don't need for your app.
    {
      provider: firebase.auth.PhoneAuthProvider.PROVIDER_ID,
      defaultCountry: 'TR',
    },

    {
      provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      // Required to enable this provider in One-Tap Sign-up.
      authMethod: 'https://accounts.google.com',
      // Required to enable ID token credentials for this provider.
      clientId: environment.clientId
    },
    {
      provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
      // Whether the display name should be displayed in Sign Up page.
      requireDisplayName: true
    },
    {
      provider: firebase.auth.FacebookAuthProvider.PROVIDER_ID,
      scopes: [
        'public_profile',
        'email',
        'user_likes',
        'user_friends'
      ]
    },
    firebase.auth.TwitterAuthProvider.PROVIDER_ID,
    firebase.auth.GithubAuthProvider.PROVIDER_ID
  ],
  // Terms of service url.
  'tosUrl': 'https://www.vebora.com',
  // Privacy policy url.
  'privacyPolicyUrl': 'https://www.vebora.com',
  // 'credentialHelper': firebaseui.auth.CredentialHelper.GOOGLE_YOLO // Not yet released
  'credentialHelper': firebaseui.auth.CredentialHelper.ACCOUNT_CHOOSER_COM
};
