import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { ChatMessage } from '../models/chat-message';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { LoadingService } from './loading.service';

@Injectable()
export class ChatService {

  messageCollection: AngularFirestoreCollection<ChatMessage>;

  _messagesSubject: Subject<ChatMessage[]> = new BehaviorSubject([]);
  _messages$: Observable<ChatMessage[]> = this._messagesSubject.asObservable();

  constructor(afs: AngularFirestore, loadingService: LoadingService) {

    loadingService.show();
    this.messageCollection = afs.collection('rooms').doc('GENERAL').collection<ChatMessage>('messages',
      ref => ref.orderBy('timestamp', 'desc').limit(30));

    this.messageCollection.valueChanges().subscribe(messages => {
      this._messagesSubject.next(messages);
      loadingService.hide();
    });
  }

  get messages$() {
    return this._messages$;
  }

  addMessage(message: ChatMessage) {
    this.messageCollection.add(message);
  }
}
