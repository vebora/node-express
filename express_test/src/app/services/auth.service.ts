import { Injectable } from '@angular/core';
import { User } from '../models/user';


import * as firebase from 'firebase/app';
import { Subject, ReplaySubject, Subscription } from 'rxjs';
import { AngularFirestore } from 'angularfire2/firestore';
import { Session } from '../models/session';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { LoginService } from './login.service';

@Injectable()
export class AuthService {

  private firstLogin = true;
  private _user: User;
  private storageUser: User;
  private _userSubject: Subject<User> = new ReplaySubject(1);
  private _auth$ = this._userSubject.asObservable();
  private userSubs: Subscription;
  private ui;
  constructor(
    private afs: AngularFirestore,
    private ms: MatSnackBar,
    private router: Router,
    private loginService: LoginService

  ) {
    console.log('Authservice start');
    this.storageUser = this.getUserFromLocalStorage();
    if (this.storageUser) {
      this._userSubject.next(this.storageUser);
    }

    this.loginService.login$.subscribe(state => {
      let user: User;
      if (state) {
        this.userSubs = this.afs.collection('users').doc<User>(state.uid).valueChanges().subscribe(fsUser => {
          let userName = state.displayName;
          let photoUrl = state.photoURL;
          let userExist = false;
          let admin = false;
          if (fsUser) {
            userExist = true;
            userName = fsUser.name;
            photoUrl = fsUser.photoUrl;
            if (fsUser.admin) {
              admin = true;
            }
          }
          user = {
            name: userName,
            email: state.email,
            id: state.uid,
            photoUrl: photoUrl ? photoUrl : ANONMOUS_USER_PHOTO,
            phoneNumber: state.phoneNumber,
            lastLogin: firebase.firestore.Timestamp.now(),
            admin: admin
          };

          if (!userExist) {
            this.saveUser(user);
          }
          this.brodcastUserIfDifferent(user);
          if (user.name && this.firstLogin) {
            this.firstLogin = false;
            this.ms.open(user.name.toUpperCase() + ' olarak oturum açıldı');
          }
        });

      } else {
          this._userSubject.next(null);
      }

    });


    this._auth$.subscribe(user => {
      this._user = user;
      if (user && user.name) {
        this.saveUserToLocalStorage(user);
      }
    });
  }

  logout() {
    this.loginService.logOut();
    this.removeUserFromLocalStorage();
    this.router.navigate(['/login']);
    this._userSubject.next(null);
    if (this.userSubs) {
      this.userSubs.unsubscribe();
    }
    this.firstLogin = true;
  }

  updateUserName(name: string) {
    this.user.name = name;
    this.saveUser(this.user);
    // this._userSubject.next(this.user);
    this.ms.open('İsminiz ' + name + ' olarak güncellendi');

  }

  updateUserPhoto(url: string) {
    this.user.photoUrl = url;
    this.saveUser(this.user);
    this.ms.open('Profil resmi güncellendi');
  }

  get user$() {
    return this._auth$;
  }
  get user(): User {
    return this._user;
  }

  saveUser(user: User) {
    user.lastLogin = firebase.firestore.Timestamp.now();
    this.afs.collection<User>('users').doc(user.id).set(user, { merge: true });
    const session: Session = {
      timestamp: firebase.firestore.Timestamp.now(),
      user_agent: navigator.userAgent,
    };
    this.afs.collection<User>('users').doc(user.id).collection('sessions').add(session);
  }

  saveUserToLocalStorage(user) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  getUserFromLocalStorage() {
    const storageUser = localStorage.getItem('user');
    if (storageUser) {
      const user: User = JSON.parse(storageUser);
      if (user.lastLogin) {
        user.lastLogin = new firebase.firestore.Timestamp(user.lastLogin.seconds, user.lastLogin.nanoseconds);
      }
      return user;
    } else {
      return null;
    }
  }

  removeUserFromLocalStorage() {
    localStorage.removeItem('user');
    this.storageUser = null;
  }

  brodcastUserIfDifferent(user: User) {
    if (!this.storageUser) {
      this._userSubject.next(user);
    } else if (
      this.storageUser.name !== user.name ||
      this.storageUser.photoUrl !== user.photoUrl ||
      this.storageUser.admin !== user.admin) {
      console.log('New user broadcasting');
      this._userSubject.next(user);
    }



  }
}

const ANONMOUS_USER_PHOTO =
  'https://storage.googleapis.com/node-test-1-208316.appspot.com/people_user_business_web_man_person_social-512.png';
