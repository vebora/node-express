import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LoadingComponent } from '../components/loading/loading.component';
import { BuildInfoComponent } from '../components/build-info/build-info.component';
import { LoginComponent } from '../components/login/login.component';

@NgModule({
  imports: [CommonModule],
  declarations: [LoadingComponent, BuildInfoComponent, LoginComponent
  ],
  exports: [LoadingComponent, BuildInfoComponent],
  providers: []
})
export class SharedModule { }
