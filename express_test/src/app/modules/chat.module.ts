import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MainNavigationComponent } from '../components/main-navigation/main-navigation.component';
import { AuthGuard } from '../guards/auth-guard';
import { UserListComponent } from '../components/user-list/user-list.component';
import { EmptyNameGuard } from '../guards/empty-name-guard';
import { MyProfileComponent } from '../components/my-profile/my-profile.component';
import { ChatComponent } from '../components/chat/chat.component';
import { PrivateChatComponent } from '../components/private-chat/private-chat.component';
import { MyChatsComponent } from '../components/my-chats/my-chats.component';
import { ChatRoomComponent } from '../components/chat-room/chat-room.component';
import { ChatMessageDisplayerComponent } from '../components/chat-room/chat-message-displayer/chat-message-displayer.component';
import { PageHeaderComponent } from '../components/page-header/page-header.component';
import { LogoutConfirmationBsComponent } from '../components/my-profile/logout-confirmation-bs/logout-confirmation-bs.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatListModule } from '@angular/material/list';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AuthService } from '../services/auth.service';
import { MyRoomsService } from '../services/my-rooms.service';
import { UsersService } from '../services/users.service';
import { ChatService } from '../services/chat.service';
import { NotificationService } from '../services/notification.service';
import { SharedModule } from './shared.module';
import {  AngularFirestoreModule } from 'angularfire2/firestore';

const routes: Routes = [
  {
    path: '',
    component: MainNavigationComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'user-list', pathMatch: 'full' },
      { path: 'user-list', component: UserListComponent, canActivate: [EmptyNameGuard] },
      { path: 'my-chats', component: MyChatsComponent, canActivate: [EmptyNameGuard] },
      { path: 'my-profile', component: MyProfileComponent }
    ],
    data: { state: 'main' }
  },
  {
    path: 'chat', component: ChatComponent, canActivate: [AuthGuard, EmptyNameGuard], data: { state: 'chat' }
  },
  {
    path: 'private-message', component: PrivateChatComponent, canActivate: [AuthGuard, EmptyNameGuard], data: { state: 'private-message' }
  },
];

@NgModule({
  declarations: [
    ChatComponent,
    UserListComponent,
    PrivateChatComponent,
    ChatRoomComponent,
    ChatMessageDisplayerComponent,
    MyChatsComponent,
    MainNavigationComponent,
    MyProfileComponent,
    PageHeaderComponent,
    LogoutConfirmationBsComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    ImageCropperModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatBottomSheetModule,
    MatListModule,

  ],
  exports: [
    RouterModule
  ],
  providers: [
    AuthService,
    ChatService,
    MyRoomsService,
    NotificationService,
    UsersService,
    AuthGuard,
    EmptyNameGuard
  ],
  entryComponents: [LogoutConfirmationBsComponent],
})
export class ChatModule {
  constructor() {
    console.log('Chat module init');
  }
}
