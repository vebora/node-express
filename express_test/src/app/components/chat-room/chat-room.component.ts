import { Component, OnInit, EventEmitter, Input, Output, OnDestroy, OnChanges, AfterViewChecked, ViewContainerRef } from '@angular/core';
import { ChatMessage } from '../../models/chat-message';
import * as firebase from 'firebase/app';
import { AuthService } from '../../services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-chat-room',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.css']
})
export class ChatRoomComponent implements OnInit, OnDestroy, OnChanges {


  @Output() message: EventEmitter<ChatMessage> = new EventEmitter();
  @Input() messages: ChatMessage[] = [];
  @Input() roomId: string;
  uname: string;
  focused = false;

  private subscription: Subscription;

  constructor(
    private authService: AuthService,
    private view: ViewContainerRef
  ) { }

  ngOnInit() {
    this.subscription = this.authService.user$.subscribe(user => this.uname = user.name);
  }

  sendMessage(event, input) {
    if (input.value.length > 0) {
      const message: ChatMessage = {
        message: input.value,
        timestamp: firebase.firestore.Timestamp.now(),
        username: this.authService.user.name,
        photo: this.authService.user.photoUrl,
        roomId: this.roomId,
        userId: this.authService.user.id,
      };
      this.message.emit(message);
      input.value = '';
    } else {
    }
    input.focused = true;
    event.preventDefault();
    return false;

  }

  ngOnChanges() {
    setTimeout(() => {
      // this.view.element.nativeElement.scrollTo(0, 10000);
      // window.scrollTo(0, 10000);
    }, 1000);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  identify(index, msg: ChatMessage) {
    return msg.username + msg.timestamp;
  }
}
