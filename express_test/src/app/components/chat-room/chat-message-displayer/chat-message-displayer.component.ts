import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { ChatMessage } from '../../../models/chat-message';

@Component({
  selector: 'app-chat-message-displayer',
  templateUrl: './chat-message-displayer.component.html',
  styleUrls: ['./chat-message-displayer.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChatMessageDisplayerComponent implements OnInit {

  @Input() msg: ChatMessage;
  constructor() { }

  ngOnInit() {
  }

}
