import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { MyRoomsService } from '../../services/my-rooms.service';
import { ChatRoom } from '../../models/chat-room';

@Component({
  selector: 'app-my-chats',
  templateUrl: './my-chats.component.html',
  styleUrls: ['./my-chats.component.css']
})
export class MyChatsComponent implements OnInit {

  rooms$: Observable<ChatRoom[]>;

  constructor(
    private router: Router,
    private mrs: MyRoomsService
  ) { }

  ngOnInit() {
    this.rooms$ = this.mrs.rooms$;
  }

  gotoChatRoom(room: ChatRoom) {
    this.router.navigate(['main', 'private-message', {
      roomId: room.id
    }]);
  }

}

