import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, AfterViewChecked } from '@angular/core';
import { User } from '../../models/user';
import { Observable } from 'rxjs';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { LogoutConfirmationBsComponent } from './logout-confirmation-bs/logout-confirmation-bs.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { tap, filter, finalize } from 'rxjs/operators';
import { AngularFireStorage } from 'angularfire2/storage';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css'],
})
export class MyProfileComponent implements OnInit, AfterViewChecked {
  user$: Observable<User>;
  editName: boolean;
  showUpload: boolean;
  @ViewChild('nameInput')
  nameInput: ElementRef;
  showCropper: boolean;

  @ViewChild('form')
  form: ElementRef;

  imageChangedEvent: any = '';
  croppedImage: any = '';


  constructor(
    private authService: AuthService,
    private mbs: MatBottomSheet,
    private ms: MatSnackBar,
    private storage: AngularFireStorage
  ) {
  }

  uploadFile(event) {
    if (event.target.files.length === 0) {
      this.form.nativeElement.reset();
      return;
    }
    this.showCropper = true;
    this.imageChangedEvent = event;

  }

  imageCroppedBase64(image: string) {
    this.croppedImage = image;
  }

  cancelUpload() {
    this.showCropper = false;
    this.imageChangedEvent = null;
    this.form.nativeElement.reset();
  }
  startUpload() {
    const base64strOrj: string = this.croppedImage;
    const base64str = base64strOrj.replace('data:image/jpeg;base64,', '');

    const metadata = {
      contentType: 'image/jpeg',
      cacheControl: 'public,max-age=86400',
    };


    const filePath = '/profiles/' + this.authService.user.id + '_profile-pic.jpeg';
    const fileRef = this.storage.ref(filePath);
    const task = fileRef.putString(base64str, 'base64', metadata);
    this.showUpload = true;
    // observe percentage changes

    task.percentageChanges().pipe(tap(perc => {
      if (perc === 100) {
        this.showUpload = false;
      }
    })).subscribe();
    // get notified when the download URL is available
    task.snapshotChanges().pipe(
      finalize(() => fileRef.getDownloadURL().subscribe(url => {
        this.authService.updateUserPhoto(url);
        this.showCropper = false;
        this.form.nativeElement.reset();

      }))
    )
      .subscribe();
  }

  ngOnInit() {
    this.user$ = this.authService.user$.pipe(
      tap(user => {
        if (user) {
          this.editName = user.name == null;
        }
      })
    );

  }

  logout() {
    this.mbs.open(LogoutConfirmationBsComponent);
  }

  updateName() {
    const name = this.nameInput.nativeElement.value;
    if (name.length > 3) {
      this.authService.updateUserName(name);
      this.editName = false;
    } else {
      this.ms.open('İsim 3 karakterden fazla olmalı');
    }

  }

  editMode() {
    this.editName = true;
  }

  ngAfterViewChecked() {
    if (this.nameInput) {
      this.nameInput.nativeElement.focus();
    }
  }
}
