import { Component, OnInit } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-logout-confirmation-bs',
  templateUrl: './logout-confirmation-bs.component.html',
  styleUrls: ['./logout-confirmation-bs.component.css']
})
export class LogoutConfirmationBsComponent implements OnInit {

  constructor(
    private bottomSheetRef: MatBottomSheetRef<LogoutConfirmationBsComponent>,
    private authService: AuthService) { }

  ngOnInit() {
  }


  yes() {
    this.authService.logout();
    this.bottomSheetRef.dismiss();


  }
  no() {
    this.bottomSheetRef.dismiss();

  }

}
