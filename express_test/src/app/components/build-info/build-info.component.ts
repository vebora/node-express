import { Component, OnInit } from '@angular/core';
import { BuildInfo } from '../../../buildinfo';

@Component({
  selector: 'app-build-info',
  templateUrl: './build-info.component.html',
  styleUrls: ['./build-info.component.css']
})
export class BuildInfoComponent implements OnInit {
  date = BuildInfo.date;
  version = BuildInfo.rev;
  constructor() { }

  ngOnInit() {
  }

}
