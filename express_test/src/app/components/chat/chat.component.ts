import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { User } from '../../models/user';
import { ChatMessage } from '../../models/chat-message';
import { ChatService } from '../../services/chat.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, OnDestroy {
  messages: ChatMessage[];
  classes = 'collection-item avatar';
  uname = '';
  focused = false;
  private subs1: Subscription;
  private subs2: Subscription;

  constructor(
    private authService: AuthService,
    private chatService: ChatService,
    private router: Router,

  ) { }


  ngOnInit() {
    this.subs1 = this.chatService.messages$.subscribe(messages => this.messages = messages);

    this.subs2 = this.authService.user$.subscribe((user: User) => {
      this.uname = user.name;
    });
  }

  sendMessage(message: ChatMessage) {
    this.chatService.addMessage(message);

  }

  ngOnDestroy() {
    this.subs1.unsubscribe();
    this.subs2.unsubscribe();
  }

  goBack() {
    this.router.navigate(['main', 'my-chats']);
  }

}

