import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Observable } from 'rxjs';

import { Router } from '@angular/router';
import { UsersService } from '../../services/users.service';
import Util from '../../utils/utils';
import { User } from '../../models/user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users$: Observable<User[]>;

  constructor(
    public authService: AuthService,
    private router: Router,
    private userService: UsersService
  ) { }

  ngOnInit() {
    this.users$ = this.userService.users$;
  }


  public personalMessageToUser(user: User) {
    const fromUser = this.authService.user;
    if (user.id !== fromUser.id) {
      const roomId = Util.createUniqueId(fromUser.id, user.id);

      this.router.navigate(['main', 'private-message', {
        roomId: roomId
      }]);
    }
  }

}
