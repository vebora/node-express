import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { LoadingService } from '../../services/loading.service';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  notLogged = false;
  private subs: Subscription;
  private retrieving = false;
  admin = false;
  private adminCounter = 0;
  constructor(
    private loginService: LoginService,
    private router: Router,
    private loader: LoadingService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loader.show();
    this.subs = this.loginService.login$.subscribe(user => {
      if (user) {
        this.router.navigate(['autoredirect']);
      } else {
        this.notLogged = true;

        if (!this.retrieving) {
          this.retrieving = true;
          this.loginService.retrieve();
        }
      }
      this.loader.hide();
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  loginAsAdmin() {
    this.loginService.loginAsAdmin();
  }

  adminTap() {
    this.adminCounter++;
    if (this.adminCounter > 10) {
      this.admin = true;
    }
  }
}
