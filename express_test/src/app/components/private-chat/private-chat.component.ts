import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { ChatMessage } from '../../models/chat-message';
import { Observable } from 'rxjs';
import { tap, take } from 'rxjs/operators';

import * as firebase from 'firebase/app';
import 'firebase/firestore';
import { User } from '../../models/user';
import { LoadingService } from '../../services/loading.service';
import { AuthService } from '../../services/auth.service';
import Util from '../../utils/utils';

@Component({
  selector: 'app-private-chat',
  templateUrl: './private-chat.component.html',
  styleUrls: ['./private-chat.component.css']
})
export class PrivateChatComponent implements OnInit {

  private messagesCollection: AngularFirestoreCollection<ChatMessage>;
  private currentRoomDoc: AngularFirestoreDocument<any>;
  private currentRoomOtherUserDoc: AngularFirestoreDocument<any>;
  private currentRoomCurrentUserDoc: AngularFirestoreDocument<any>;

  private currentUserCurrentRoomDoc: AngularFirestoreDocument<any>;
  private otherUserCurrentRoomDoc: AngularFirestoreDocument<any>;

  messages$: Observable<ChatMessage[]>;
  toUser$: Observable<User>;
  roomId = 'not_asigned_yet';

  private toUser: User;
  private toUserId: string;
  private firstMessage: boolean;
  private fromUserId: string;


  constructor(
    private route: ActivatedRoute,
    private afs: AngularFirestore,
    private loader: LoadingService,
    private as: AuthService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.loader.show();
    this.route.params.subscribe(params => {
      this.roomId = params.roomId;
      this.fromUserId = this.as.user.id;
      this.toUserId = Util.getOtherUserId(this.roomId, this.fromUserId);

      const roomsCollection = this.afs.collection('rooms');
      this.currentRoomDoc = roomsCollection.doc(this.roomId);

      this.currentRoomCurrentUserDoc = this.currentRoomDoc.collection('users').doc(this.fromUserId);

      this.toUser$ = this.afs.collection('users').doc<User>(this.toUserId).valueChanges().pipe(take(1), tap(user => this.toUser = user));
      // take(1) will prevent peer user info from updating. For now it is not important
      this.messagesCollection = this.currentRoomDoc.collection('messages', ref => ref.orderBy('timestamp', 'desc'));

      this.messages$ = this.messagesCollection.valueChanges().pipe(
        tap(messages => {
          if (messages.length > 0) {
            this.currentRoomCurrentUserDoc.set({ lastRead: firebase.firestore.Timestamp.now() }, { merge: true });
            this.firstMessage = false;
          } else {
            this.firstMessage = true;
          }
        }),
        tap(() => this.loader.hide()));
    });
  }

  sendMessage(message: ChatMessage) {
    this.messagesCollection.add(message).then(e => {
    });
    if (this.firstMessage) {
      this.createRooms();
    }
  }


  createRooms() {
    this.currentRoomCurrentUserDoc.set({ id: this.fromUserId }, { merge: true });

    this.currentRoomOtherUserDoc = this.currentRoomDoc.collection('users').doc(this.toUserId);
    this.currentRoomOtherUserDoc.set({ id: this.toUserId }, { merge: true });

    this.currentUserCurrentRoomDoc = this.afs.collection('users').doc(this.fromUserId).collection('rooms').doc(this.roomId);
    this.otherUserCurrentRoomDoc = this.afs.collection('users').doc(this.toUserId).collection('rooms').doc(this.roomId);

    const fromUser = this.as.user;
    this.otherUserCurrentRoomDoc.set(
      {
        id: this.roomId,
        roomName: fromUser.name,
        photoUrl: fromUser.photoUrl,
      },
      { merge: true });

    this.currentUserCurrentRoomDoc.set(
      {
        id: this.roomId,
        roomName: this.toUser.name,
        photoUrl: this.toUser.photoUrl,
      },
      { merge: true });

  }
  goBack() {
    this.router.navigate(['main', 'my-chats']);
  }

}

