import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { NotificationService } from '../../services/notification.service';

@Component({
  selector: 'app-main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.css']
})
export class MainNavigationComponent implements OnInit {

  count$: Observable<number>;

  constructor(
    private notificatinonService: NotificationService,
  ) { }
  ngOnInit(): void {
    this.count$ = this.notificatinonService.notificationCount$;

  }


}
