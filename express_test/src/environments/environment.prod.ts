export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCbNdTHSf7jRra9qtafSVJb-XRQHSiHHaQ',
    authDomain: 'angular-fire-test-3ce4e.firebaseapp.com',
    databaseURL: 'https://angular-fire-test-3ce4e.firebaseio.com',
    projectId: 'angular-fire-test-3ce4e',
    storageBucket: 'prod-default-bucket',
    messagingSenderId: '802617105245'
  },
  clientId: '802617105245-k0b62jkgcl8mig36r0u75qu23fknsr8u.apps.googleusercontent.com'
};
