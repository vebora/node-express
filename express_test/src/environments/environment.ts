// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCw4RQYZ1wMIqQAWSRN1r8P7KkT1fJWmi8',
    authDomain: 'angular-fire-test-dev.firebaseapp.com',
    databaseURL: 'https://angular-fire-test-dev.firebaseio.com',
    projectId: 'angular-fire-test-dev',
    storageBucket: 'dev-default-bucket',
    messagingSenderId: '990043292475'
  },
  clientId: '990043292475-1d7cnhdgimi17ddkl4prb4dlpcu7co95.apps.googleusercontent.com',
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
