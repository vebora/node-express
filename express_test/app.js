const express = require("express");
const path = require("path");

const app = express();
app.use(express.static(path.join(__dirname, '/dist')));

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname + '/dist/index.html'));
});

app.get('*', function (req, res) {
  res.sendFile(path.join(__dirname + '/dist/index.html'));
});

var server = app.listen(process.env.PORT || 3000, () => {
  var host = server.address().address;
  var port = server.address().port;
  console.log('Example app listening at port %s', port);
});
