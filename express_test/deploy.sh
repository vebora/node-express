npm run create-version &&
npm run build-for-prod &&
gcloud.cmd app deploy --version $(gcloud.cmd app versions list --sort-by="TRAFFIC_SPLIT" --limit=1 --format="value(id)") --no-promote --quiet
 