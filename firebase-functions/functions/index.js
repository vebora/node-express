const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();
const db = admin.firestore();
db.settings({
    timestampsInSnapshots: true
});


exports.onMessage = functions.firestore
    .document('rooms/{roomId}/messages/{messageId}')
    .onCreate(async (snap, context) => {
        try {
            const roomId = context.params.roomId;
            const message = snap.data();
            const senderId = snap.data().userId;
            const snapshot = await db.collection("rooms").doc(roomId).collection("users").get();

            const promises = [];
            snapshot.docs.forEach(
                userDoc => {
                    const promise = checkUnread(userDoc.data(), roomId, senderId, message);
                    promises.push(promise);
                })

            return Promise.all(promises);

        } catch (error) {
            err => console.log(err);
            return false;
        }
    });

exports.onUserRead = functions.firestore
    .document('rooms/{roomId}/users/{userId}')
    .onUpdate(async (change, context) => {
        try {
            const roomId = context.params.roomId;
            const userId = context.params.userId;
            const userData = change.after.data();

            const messages = await db.collection("rooms").doc(roomId).collection("messages").orderBy("timestamp", "desc").limit(1).get()
            const message = messages.docs[0].data();
            return checkUnread(userData, roomId, userId, message)
        } catch (error) {
            error => console.log(error)
            return false;
        }


    });

function checkUnread(userData, roomId, senderId, message) {

    const userId = userData.id;
    const roomDoc = db.collection("users").doc(userId).collection("rooms").doc(roomId);
    let hasUnread = false;
    if (userId === senderId) {
        hasUnread = false;
    } else if (!userData.lastRead) {
        hasUnread = true;
    } else {
        hasUnread = message.timestamp.toDate() > userData.lastRead.toDate();
    }
    const promise = roomDoc.set({
        id: roomId,
        lastMessage: {
            message: message.message,
            timestamp: message.timestamp
        },
        hasUnread: hasUnread,
    }, {
        merge: true
    });
    console.log("Unread checked result " + hasUnread + " for user " + userId + " message :" + message.message)
    return promise;
}